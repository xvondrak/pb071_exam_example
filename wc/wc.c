#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

struct file {
	char *name;
	int word_count;
	int line_count;
	int byte_count;
};

enum sort_key {
	WORD_COUNT_O,
	LINE_COUNT_O,
	BYTE_COUNT_O,
	LEXIC_O,
};

enum read_state {
	READ_OK,
	READ_FAIL,
};

int sort_rule_lexic(const void *a, const void *b) {
	// must be more complex on whole word
	return (((struct file *) a)->name[0]) - (((struct file *) b)->name[0]);
}

int sort_rule_byte(const void *a, const void *b) {
	return (((struct file *) a)->byte_count) - (((struct file *) b)->byte_count);
}

int sort_rule_line(const void *a, const void *b) {
	return (((struct file *) a)->line_count) - (((struct file *) b)->line_count);
}

int sort_rule_word(const void *a, const void *b) {
	return (((struct file *) a)->word_count) - (((struct file *) b)->word_count);
}

void sort_files(struct file *files, size_t files_num, enum sort_key sort_key) {
	int (*comp)(const void *a, const void *b);
	switch (sort_key) {
		case WORD_COUNT_O:
			comp = sort_rule_word;
			break;
		case LINE_COUNT_O:
			comp = sort_rule_line;
			break;
		case BYTE_COUNT_O:
			comp = sort_rule_byte;
			break;
		default:
			comp = sort_rule_lexic;
	}
	qsort(files, files_num, sizeof(struct file), comp);	
}

// if file_name==NULL, read from stdin
enum sort_key read_file(char *file_name, struct file *file_info) {
	FILE *input = file_name == NULL ? stdin : fopen(file_name, "r");
	if (input == NULL)
		return READ_FAIL;
	
	file_info->name = malloc(sizeof(char) * 
			strlen(file_name == NULL ? "stdin" : file_name) + 1);
	if (file_info->name == NULL) {
		fclose(input);
		return READ_FAIL;
	}
	memcpy(file_info->name, 
			file_name == NULL ? "stdin\0" : file_name, 
			strlen(file_name == NULL ? "stdin" : file_name) + 1);
	file_info->word_count = 0;
	file_info->line_count = 0;
	file_info->byte_count = 0;
	
	bool last_is_letter = false;
	char cur;
	while ((cur = fgetc(input)) != EOF) {
		file_info->byte_count++;
		if (cur == ' ') {
			if (last_is_letter)
				file_info->word_count++;
			else
				continue;
			last_is_letter = false;
		} else if (cur == '\n') {
			file_info->line_count++;
		} else {
			last_is_letter = true;
		}
	}

	file_info->line_count++; // add ending line
	if (file_info->byte_count != 0)
		file_info->word_count++; // ending word

	fclose(input);
	return READ_OK;
}

enum sort_key read_files(struct file *files, size_t files_num, char **file_names) {
	for (int i = 0; i < files_num; i++) {
		if (read_file(file_names[i+1], files+i) == READ_FAIL)
			return READ_FAIL;
	}
	return READ_OK;
}

void print_files(struct file *files, size_t files_num) {
	printf("\n");
	for (int i = 0; i < files_num; i++) {
		printf("%d:\t%s\t%d\t%d\t%d\n", 
				i, 
				files[i].name, 
				files[i].word_count, 
				files[i].byte_count, 
				files[i].line_count);
	}
}

void destroy_files(struct file *files, size_t files_num) {
	for (int i = 0; i < files_num; i++) {
		if (files[i].name != NULL)
			free(files[i].name);
	}
	free(files);
}

int main(int argc, char **argv) {
	// parse arguments
	enum sort_key sort_key = LEXIC_O;
	if (argc > 1) {
		if (strcmp(argv[1], "-s"))
			sort_key = BYTE_COUNT_O;
		else if (strcmp(argv[1], "-l"))
			sort_key = LINE_COUNT_O;
		else if (strcmp(argv[1], "-w"))
			sort_key = WORD_COUNT_O;
	}

	size_t files_num = sort_key == LEXIC_O ? argc - 1 : argc - 2;
	struct file *files = malloc((files_num == 0 ? 1 : files_num) * 
			sizeof(struct file)); 
	if (files == NULL)
		return EXIT_FAILURE;

	// decide whether read from stdin or from files
	enum read_state read_state = READ_OK;
	if (files_num > 0) {
	        read_state = read_files(files, files_num, argv + argc - files_num - 1);
	} else {
		read_state = read_file(NULL, &(files[0]));
		files_num++;
	}

	if (read_state != READ_OK) {		
		printf("cannot read some of files\n");
		destroy_files(files, files_num);
		return EXIT_FAILURE;
	}

	sort_files(files, files_num, sort_key);

	print_files(files, files_num);
	
	destroy_files(files, files_num);

	return EXIT_SUCCESS;
}

